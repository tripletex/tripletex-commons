# unmap, untangles obscured error messages due to minified source code


This utility makes it easier to debug javascript error messages in those cases
where the source code has been minified.

## Install

Install this software by typing the following:

```bash
git clone git@bitbucket.org:tripletex/tripletex-commons.git
cd tripletex-commons
cd unmap
npm install -g
```


## Use

Let us say the error message from the browser console/kibana log looks like this:


> message: Uncaught ReferenceError: SomethingThatDoesntExist is not
> defined, file: http://localhost:8080/js/common.js, line: 2, colno: 14077

Then you can type the following:

```bash
unmap common.js 2 14077
```

The result may look something like this:

```json
{ "source": "/source/c-common.js",
  "line": 437,
  "column": 11,
  "name": "SomethingThatDoesntExist" }
```
