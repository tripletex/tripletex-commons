#!/usr/bin/env node

'use strict'

const fs = require('fs')
const sourceMap = require('source-map')

const args = process.argv

let filename = args[2]
let line = parseInt(args[3])
let column = parseInt(args[4])


let jsfile, mapname, mapfile, consumer, answer

if(filename === undefined || isNaN(line) || isNaN(column)) {
	console.log('')
	console.log('Usage: unmap <js-file> <line> <column>')
	process.exit(1)
}


jsfile = fs.readFileSync(filename, 'utf8')

jsfile.split('\n').forEach(line => {
	if(line.indexOf('//# sourceMappingURL=') > -1) {
		let start = line.indexOf('//# sourceMappingURL=') + '//# sourceMappingURL='.length
		mapname = line.substring(start)
	}
})

mapfile = fs.readFileSync(mapname, 'utf8')


consumer = new sourceMap.SourceMapConsumer(mapfile)

answer = consumer.originalPositionFor({line, column})

process.stdout.write(JSON.stringify(answer))
