/*
 * Copyright 2015  Tripletex AS.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.tripletex.commons.filter;

/**
 * Class that contaqines information about an resource, at the moment this
 * information is only the sha1 hash of the content and the timestamp of the
 * file if we fetch the timestamp.
 *
 * @author Christian Andersson
 */
public class StaticResourceData {

    public final long timestamp;
    public final String hash;

    public StaticResourceData(long timestamp, String hash) {
        this.timestamp = timestamp;
        this.hash = hash;
    }
}
