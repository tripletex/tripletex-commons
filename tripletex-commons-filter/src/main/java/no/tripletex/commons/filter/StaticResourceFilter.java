/*
 * Copyright 2015 Tripletex AS.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.tripletex.commons.filter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Filter that is used to setup better Caching for static resources in
 * tripletex.no
 *
 * When a request comes in that tries to fetch an resource, we first tries to
 * see if we have calculated an hash for this reasource, if we have not, then we
 * calculate an hash. Once we have the hash for the resource, we set this hash
 * in the response as the etag (this will be used later request for the same
 * resource) IF the request has sent with it an If-None-Match header, it means
 * that we should check the value of that header against our hash for this
 * resource, if they are equal, then we can send back that the resource is an
 * "NOT MODIFIED", and the browser can instead use the cache. if they are not
 * equal, then we send back the con tent of the request along with the new etag.
 *
 *
 *
 * @author Christian Andersson
 *
 */
public class StaticResourceFilter implements Filter {

    private Map<String, StaticResourceData> calculatedHashes = null;

    private boolean useTimeStamp = false;
    private static Long maxAge = null;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;
        ServletContext context = request.getServletContext();

        String suppliedEtag = request.getHeader("If-None-Match");
        String uri = request.getRequestURI();

        StaticResourceData storedCache = getData(context, uri);

        if (storedCache != null) {
            response.setHeader("Etag", storedCache.hash);
            StringBuilder cacheControl = new StringBuilder("no-cache");
            if (maxAge != null && maxAge >= 0) {
                cacheControl.append(", max-age=").append(maxAge);
            }
            response.setHeader("Cache-Control", cacheControl.toString());
            response.setHeader("Pragma", null);
            response.setHeader("Expires", null);
            if (storedCache.hash.equals(suppliedEtag)) {
                response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                return;
            }
        }

        chain.doFilter(request, new HttpServletResponseWrapper(response) {
            @Override
            public void addHeader(String name, String value) {
                if (!"etag".equalsIgnoreCase(name)
                        && !"Cache-Control".equalsIgnoreCase(name)
                        && !"Expires".equalsIgnoreCase(name)
                        && !"Pragma".equalsIgnoreCase(name)) {
                    super.addHeader(name, value);
                }
            }

            @Override
            public void setHeader(String name, String value) {
                if (!"etag".equalsIgnoreCase(name)
                        && !"Cache-Control".equalsIgnoreCase(name)
                        && !"Expires".equalsIgnoreCase(name)
                        && !"Pragma".equalsIgnoreCase(name)) {
                    super.setHeader(name, value);
                }
            }
        });
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        calculatedHashes = new ConcurrentHashMap<>();
        String initUseTimeStamp = config.getInitParameter("useTimeStamp");
        if (initUseTimeStamp != null && (initUseTimeStamp.equalsIgnoreCase("Y")
                || initUseTimeStamp.equalsIgnoreCase("YES")
                || initUseTimeStamp.equalsIgnoreCase("TRUE"))) {
            this.useTimeStamp = true;
        }
        String initMaxAge = config.getInitParameter("maxAge");
        if (maxAge != null) {
            try {
                maxAge = Long.valueOf(initMaxAge);
            } catch (NumberFormatException e) {
                // Do nothing
            }
        }
    }

    @Override
    public void destroy() {
        calculatedHashes = null;
    }

    /**
     * Rr
     *
     * @param context
     * @param uri
     * @return
     */
    private StaticResourceData getData(ServletContext context, String uri) {
        try {
            long timestamp = 0;

            StaticResourceData data = calculatedHashes.get(uri);
            if (data != null) {
                if (useTimeStamp) {
                    String realPath = context.getRealPath(uri);
                    if (realPath != null) {
                        File f = new File(realPath);
                        if (f.exists() && f.isFile()) {
                            timestamp = f.lastModified();
                        }
                    }
                }
                if (timestamp == data.timestamp) {
                    return data;
                }
            }
            try(InputStream stream = context.getResourceAsStream(uri)) {
                if (stream == null)
                    return null;
                String etag = calculateWeakEtag(stream);
                data = new StaticResourceData(timestamp, etag);
                calculatedHashes.put(uri, data);
                return data;
            }
        } catch (Exception e) {
            return null;
        }

    }

    private String calculateWeakEtag(InputStream stream) throws IOException {
        String hash = DigestUtils.sha1Hex(stream);
        return "W/\"0" + hash + "\"";
    }
}
