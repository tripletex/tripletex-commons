#!/usr/bin/env node

/**
 * This little script checks if a property file is sorted.
 * It disregards comments and blank lines.
 *
 * Install it by typing "npm install -g" while being in same
 * location as this file.
 *
 * @auth Lars-Erik Bruce
 * @date 08/16/2016
 */

const fs = require('fs')

if (process.argv.length < 3) {
	console.log('Usage: propsort <path-to-file-to-sort>')
	process.exit(1)
}


function checkFile(file) {
	fs.readFile(file, 'utf8', (err, data) => {
		if (err) throw err
		let last = ''

		data.split('\n').forEach(function(val, i) {
			if(val.startsWith('#')) return // Skip comments
			if(val.length === 0) return // Skip blank lines

			val = val.split('=')[0]
			if(last > val) {
				console.log(file + ' not sorted: ' + last + ' < ' + val)
				process.exit(1)
			}
			last = val
		})
	})
}

process.argv.slice(2).forEach(function(val){
	console.log('Checking ' + val + '.')
	checkFile(val)
})
