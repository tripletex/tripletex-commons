propsort
========

This nifty command line tool lets you check if one or more
property file are sorted.


To install, stand inside the project and write
```
npm install -g
```

To check if a property file is sorted, use

```
propsort path-to-file
```

You can indeed use wildcards in the path to check several files at once.

Since this command line tool exits with a status 1, you can indeed check
if files are sorted before pushing code to repo. Put this in your git
pre-push hook:

```
#!/bin/bash

propsort $TRIPLETEX_HOME/app/src/main/resources/ApplicationResources*.properties
```


Cheers!
