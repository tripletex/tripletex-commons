#!/usr/bin/env node

const fs = require('fs')
const dir = require('node-dir')

if (process.argv.length < 3) {
	console.log('Usage: unused <path-to-tripletex-home>')
	process.exit(1)
}

fs.readFile(process.argv[2] + 'app/src/main/resources/ApplicationResources.properties', 'utf8', (err, data) => {
	if (err) throw err
	let last = ''

	var keys = []

	data.split('\n').forEach(function(val, i) {
		if(val.startsWith('#')) return // Skip comments
		if(val.length === 0) return // Skip blank lines

		val = val.split('=')[0]
		keys.push(val)
	})

	dir.readFiles(process.argv[2],
		{
			match: /.js$|.jsp$|.java$/,
			excludeDir: [
				'node_modules',
				'systemtest',
				'target',
				'javascript-testcases',
				'sparebank1',
				'lib',
				'integrations']
		},
		function(err, content, filename, next) {
			if (err) throw err
			for(var i = 0; i < keys.length; i++) {
				if(content.indexOf(keys[i]) > -1) {
					console.log('found instance of ' + keys[i])
					keys.splice(i--, 1)
					console.log('number of remaining keys is ' + keys.length)
				}
			}
			next()
		},
		function(err, files){
			if (err) throw err
			console.log('finished reading files:', files.length)
			for(var i = 0; i < keys.length; i++) {
				console.log(keys[i])
			}
		})
})



console.log(process.argv[2] + 'app/src/main/resources/ApplicationResources.properties')
