Git TODO Application
====================

This nifty command line tool lets you add and remove TODOs
for you current repository in your current branch. You can
add, see and delete TODOs. You cannot mark TODOs as done.
Just delete them, they are done and out of the way!


To install, stand inside the project and write
```
npm install -g
```

To see a list of TODOs in your current repo and current branch:

```
todo
```

To add a message:

```
todo add I must remember to change the colors for the header.
```

To delete a message:

```
todo del 1
```

To see all TODOs in current repo:

```
todo repo
```

To see all TODOs registered with git-todo-app:

```
todo all
```


Options
-------

You can specify a branch or a repo with the -b and -r options. For
instance, to add a TODO for the branch master, you can write:


```
todo add -b master Remember to update README.md before publishing new version.
```

OBS!

The repository is identified with the local path to the repo. If you
have several clones of same repo on your disk drive, you will have one
TODO list per clone.

The TODO file is named git-todos.json and lives inside your HOME
directory. Or USERPROFILE if you are on a win32 platform, BUT we use
technology not supported by windows (child-process-promise). If you use
windows and want windows-support you are welcome to provide a pull
request.