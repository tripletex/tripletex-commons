#!/usr/bin/env node


const program = require('commander')
const exec = require('child-process-promise').exec
const fs = require('fs')

const HOME_FOLDER = process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME']

const TODO_FILE = HOME_FOLDER + '/git-todos.json'


// If todos file doesn't exist, create an empty one
if(!fs.existsSync(TODO_FILE)) {
	fs.writeFileSync(TODO_FILE, '{}')
}


var todos = require(TODO_FILE)

function add(message, path, branch) {
	if(!todos[path]) {
		todos[path] = {}
	}

	if(!todos[path][branch]) {
		todos[path][branch] = []
	}

	todos[path][branch].push(message)

	fs.writeFileSync(TODO_FILE, JSON.stringify(todos))
}

function del(id, path, branch) {
	
	if(!todos[path] || !todos[path][branch]) {
		console.log('No todos added for this branch yet!')
		return
	}

	if(id + 1 > todos[path][branch].length) {
		console.log('This TODO is not defined!')
		return
	}

	var oldMessage = todos[path][branch][id]

	todos[path][branch].splice(id, 1)

	if(todos[path][branch].length === 0) {
		// If a branch has no todos, delete the branch key from repo.
		delete todos[path][branch]
		if(Object.keys(todos[path]).length === 0) {
			// If repo has no branches, delete the repo from todos.
			delete todos[path]
		}
	}

	fs.writeFileSync(TODO_FILE, JSON.stringify(todos))

	console.log('Deleted the following TODO:')
	console.log('"' + oldMessage + '"')
}

function print(path, branch) {
	if(
	    !todos[path] ||
	    !todos[path][branch] ||
	    todos[path][branch].length === 0) {
		console.log('No todos added for this branch yet!')
		return
	}

	todos[path][branch].forEach((val, i) => {
		console.log(i + ': ' + val)
	})
}

function repo(path) {
	for (var branch in todos[path]) {
		console.log('\nTODOs for branch "' + branch + '":')
		print(path, branch)
	}
}

function all() {
	for(var repoName in todos) {
		console.log('')
		console.log(repoName)
		console.log(repoName.split('').map(a => '=').join(''))
		repo(repoName)
		console.log('')
	}
}

var command, messageOrId

program
	.arguments('<command>, [messageOrId...]')
	.option('-b, --branch <branch>', 'Add or delete message for a given branch')
	.option('-r, --repo <repo>', 'Add or delete message for a given repository (given by local path)')
	.action((commandArg, messageOrIdArg) => {
		command = commandArg
		messageOrId = messageOrIdArg
	})
	.parse(process.argv)


var path, branch

exec('git rev-parse --show-toplevel')
	.then(result => {
		path = program.repo || result.stdout.trim()
		return exec('git symbolic-ref --short HEAD')
	}).then(result => {
		branch = program.branch || result.stdout.trim()

		if(command === 'add' && messageOrId.length > 0) {
			add(messageOrId.join(' '), path, branch)
			console.log('message added to branch ' + branch)
			return
		}

		if(command === 'del' && messageOrId.length > 0) {
			var id = parseInt(messageOrId[0])
			if(isNaN(id)) {
				console.log('Please provide the index number of the TODO to delete')
			}

			del(id, path, branch)
			return
		}

		if(command === 'repo') {
			repo(path)
			return
		}

		if(command === 'all') {
			all()
			return
		}

		print(path, branch)
	})